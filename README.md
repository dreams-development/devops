# DevOps
## Articles
- [Continuous integration vs. continuous delivery vs. continuous deployment](https://www.atlassian.com/continuous-delivery/ci-vs-ci-vs-cd)
### Docker & NodeJS
- [A Beginner-Friendly Introduction to Containers, VMs and Docker](https://towardsdatascience.com/deploy-a-nodejs-microservices-to-a-docker-swarm-cluster-docker-from-zero-to-hero-464fa1369ea0)
- [How Docker Can Help You Become A More Effective Data Scientist](https://towardsdatascience.com/how-docker-can-help-you-become-a-more-effective-data-scientist-7fc048ef91d5)
- [My Docker Cheat Sheet](https://medium.com/statuscode/dockercheatsheet-9730ce03630d)
- [Node.js, PM2, Docker & Docker-compose DevOps](https://medium.com/@adriendesbiaux/node-js-pm2-docker-docker-compose-devops-907dedd2b69a)
- [Deploy Nodejs microservices to a Docker Swarm Cluster](https://towardsdatascience.com/deploy-a-nodejs-microservices-to-a-docker-swarm-cluster-docker-from-zero-to-hero-464fa1369ea0)
- [Полное практическое руководство по Docker: с нуля до кластера на AWS](https://habr.com/post/310460/)

